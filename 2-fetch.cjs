const fs = require("fs")
const path = require("path")
const usersDataPath = path.join(__dirname, "./usersData.json")
const todosDataPath = path.join(__dirname, "./todosData.json")
const problem4OutputPath = path.join(__dirname, "./problem4OutputPath.json")
const problem5OutputPath = path.join(__dirname, "./problem5OutputPath.json")


/*
NOTE: Do not change the name of this file

* Ensure that error handling is well tested.
* If there is an error at any point, the subsequent solutions must not get executed.
* Solutions without error handling will get rejected and you will be marked as having not completed this drill.

* Usage of async and await is not allowed.

Users API url: https://jsonplaceholder.typicode.com/users
Todos API url: https://jsonplaceholder.typicode.com/todos

Users API url for specific user ids : https://jsonplaceholder.typicode.com/users?id=2302913
Todos API url for specific user Ids : https://jsonplaceholder.typicode.com/todos?userId=2321392

Using promises and the `` library, do the following. 

1. Fetch all the users
2. Fetch all the todos
3. Use the promise chain and fetch the users first and then the todos.
4. Use the promise chain and fetch the users first and then all the details for each user.
5. Use the promise chain and fetch the first todo. Then find all the details for the user that is associated with that todo

NOTE: If you need to install `node-fetch` or a similar libary, let your mentor know before doing so along with the reason why. No other exteral libraries are allowed to be used.

Usage of the path libary is recommended
fetch

*/

let usersUrl = 'https://jsonplaceholder.typicode.com/users'
let todosUrl = 'https://jsonplaceholder.typicode.com/todos'


function fetchUrl(url) {
  return fetch(url)
    .then((response) => {

      if (response.ok) {
        return response.json()
      } else {
        return response.status
      }
    })

}


function writeFile(path, fileData) {

  return new Promise((resolve, reject) => {

    fs.writeFile(path, JSON.stringify(fileData), "utf8", (err) => {

      if (err) {
        reject(err)
      } else {
        console.log("successfully written")
        resolve()
      }
    })
  })
}

// 1. Fetch all the users

function problem1(usersDataPath, usersUrl) {

  fetchUrl(usersUrl)

    .then((usersData) => {
      return writeFile(usersDataPath, usersData)
    })

    .catch((err) => {
      console.error(err)
    })
}


// 2. Fetch all the todos

function problem2(todosDataPath, todosUrl) {

  fetchUrl(todosUrl)

    .then((todosData) => {
      return writeFile(todosDataPath, todosData)
    })

    .catch((err) => {
      console.error(err)
    })
}


// 3. Use the promise chain and fetch the users first and then the todos.

function problem3(usersUrl, todosUrl) {

  fetchUrl(usersUrl)

    .then(usersData => {
      return writeFile(usersDataPath, usersData)
    })

    .then(() => {
      return fetchUrl(todosUrl)
    })

    .then(todosData => {
      return writeFile(todosDataPath, todosData)
    })

    .catch(error => {
      console.error(error)
    });
}


// 4. Use the promise chain and fetch the users first and then all the details for each user.

function problem4(usersUrl) {

  fetchUrl(usersUrl)

    .then(usersData => {
      let checkList = usersData.map(userData => {
        return fetchUrl(`https://jsonplaceholder.typicode.com/users?id=${userData.id}`)
      })

      return Promise.all(checkList)
    })

    .then(data => {
      if (data) {
        console.log("problem 4 function successfully created")
        return writeFile(problem4OutputPath, data)
      }
    })
    .catch(err => {
      console.error(err)
    })
}


// 5. Use the promise chain and fetch the first todo. Then find all the details for the user that is associated with that todo

function problem5(todosUrl) {

  fetchUrl(todosUrl)

    .then(todosData => {

      let checkList = todosData.map(todoData => {
        return fetchUrl(`https://jsonplaceholder.typicode.com/todos?userId=${todoData.id}`)
      })

      return Promise.all(checkList)
    })

    .then(data => {

      if (data) {
        console.log("problem 5 function successfully created")
        return writeFile(problem5OutputPath, data)
      }
    })
    .catch(err => {
      console.error(err)
    })

}


// all functions are called here

problem1(usersDataPath, usersUrl)

problem2(todosDataPath, todosUrl)

problem3(usersUrl, todosUrl)

problem4(usersUrl)

problem5(todosUrl)